#include<stdio.h>
#include "ast.h"
#include "visitor.h"
#include "c_codegen.h"

extern "C" FILE *yyin;
extern "C" int yyparse();

AST_program *mainprog;

int main(int argc, char *argv[]){
	if(argc == 1){
		fprintf(stderr, "Correct usage: bcc filename\n");
		exit(1);
	}

	if(argc > 2){
		fprintf(stderr, "Passing more arguments than necessary.\n");
		fprintf(stderr, "Correct usage: bcc filename\n");
	}

	yyin = fopen(argv[1], "r");

	if(!yyin){
	    dprintf(2, "Unable to read file: %s\n", argv[1]);
	}

	yyparse();

	C_codegen *cgenvisitor = new C_codegen("gencode.c");

	mainprog->accept(cgenvisitor);
	
	printf("Written to gencode.c\n");

	return 0;
}
