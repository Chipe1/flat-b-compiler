%{
  #include "ast.h"

  using namespace std;

  extern "C" FILE * yyin;
  extern "C" int yylex();
  extern "C" int yyparse();

  enum BinaryOP getbinop(string);
  enum CmpOp getcmpop(string);
  enum LogicOP getlogicop(string);
  void yyerror (char const *s);

  extern AST_program *mainprog;
%}

%union{
	char *sval;
	int ival;

	enum Datatype dtype;
	enum UnaryOP unop;
	enum BinaryOP binop;
	enum CmpOp cmpop;
	enum LogicOP logicop;
	AST_program* program;
	AST_decl* decl;
	AST_id* id;
	AST_location* location;
	AST_expr* expr;
	AST_bool_expr* bool_expr;
	AST_label* label;
	AST_IO* io;
	AST_print* print;
	AST_statement* statement;

	vector< AST_decl* >* decl_list;
	vector< AST_id* >* id_list;
	vector< AST_print* >* print_list;
	vector< AST_statement* >* statement_list;
}

%token declaration_starter
%token code_starter
%token <ival> NUM
%token <sval> STR_LIT
%token INT
%token WHILE
%token FOR
%token IF
%token ELSE
%token GOTO
%token GOT
%token PRINT
%token PRINTLN
%token READ
%token <sval> ID
%token ETOK
%token <sval> A_OP
%token <sval> C_OP
%token <sval> L_OP
%left A_OP
%left C_OP
%left L_OP

%type <program> program
%type <decl_list> decl_block
%type <decl_list> decl_list
%type <decl> decl_line
%type <id_list> var_list
%type <id> var
%type <dtype> d_type
%type <expr> expr
%type <bool_expr> bool_expr
%type <location> location
%type <label> label
%type <print> print_item
%type <print_list> print_list
%type <io> io_statement
%type <statement_list> code_block
%type <statement_list> statement_list
%type <statement> statement

%%

program	: declaration_starter decl_block code_starter code_block {$$ = new AST_program($2, $4);mainprog = $$;}
	;

decl_block	: '{' decl_list '}' {$$ = $2;}
		| '{' '}' {$$ = new vector< AST_decl* >();}
		;

decl_list	: decl_list decl_line{$1->push_back($2);$$ = $1;}
		| decl_line {$$ = new vector< AST_decl* >();$$->push_back($1);}
         	;

decl_line	: d_type var_list ';' {$$ = new AST_decl($1, $2);}
		;

d_type		: INT {$$ = int_type;}
		;

var_list	: var_list ',' var {$1->push_back($3); $$ = $1;}
		| var {$$ = new vector< AST_id* >(); $$->push_back($1);}
		;

var		: ID '[' NUM ']' {$$ = new AST_id(string($1), $3);}
		| ID {$$ = new AST_id(string($1));}
		;

code_block	: '{' statement_list '}' {$$ = $2;}
		| '{' '}' {$$ = new vector< AST_statement* >();}
		;


statement_list	: statement_list statement {$1->push_back($2);$$ = $1;}
		| statement {$$ = new vector< AST_statement* >();$$->push_back($1);}
		;

statement	: location '=' expr ';' {$$ = new AST_assign($1, $3);}
		| expr ';' {$$ = new AST_expr_statement($1);}
		| io_statement ';' {$$ = new AST_io_statement($1);}
		| code_block {$$ = new AST_codeblock($1);}
		| FOR ID '=' NUM ',' NUM code_block {$$ = new AST_for(string($2), $4, $6, $7);}
		| FOR ID '=' NUM ',' NUM ',' NUM code_block {$$ = new AST_for(string($2), $4, $6, $8, $9);}
		| IF bool_expr code_block {$$ = new AST_if($2, $3);}
		| IF bool_expr code_block ELSE code_block {$$ = new AST_if($2, $3, $5);}
		| WHILE bool_expr code_block {$$ = new AST_while($2, $3);}
		| label ':' statement {$$ = new AST_label_statement($1, $3);}
		| GOT label ';' {$$ = new AST_goto($2);}
		| GOTO label  IF bool_expr ';' {$$ = new AST_goto($2, $4);}
		;

location	: ID '[' expr ']' {$$ = new AST_idx_location(string($1), $3);}
		| ID {$$ = new AST_id_location(string($1));}
		;

label		: ID {$$ = new AST_label(string($1));}
		;

io_statement	: PRINTLN print_list {$$ = new AST_println_statement($2);}
		| PRINT print_list {$$ = new AST_print_statement($2);}
		| READ location {$$ = new AST_read_statement($2);}
		;

print_list	: print_list ',' print_item {$1->push_back($3);$$ = $1;}
		| print_item {$$ = new vector< AST_print* >();$$->push_back($1);}
		;

print_item	: expr {$$ = new AST_print_expr($1);}
		| STR_LIT {$$ = new AST_print_str(string($1));}
		;

bool_expr	: bool_expr L_OP bool_expr {$$ = new AST_bool_logic_expr(getlogicop(string($2)), $1, $3);}
		| '(' bool_expr ')' {$$ = $2;}
		| expr C_OP expr {$$ = new AST_bool_cmp_expr(getcmpop(string($2)), $1, $3);}
		;

expr		: expr A_OP expr {$$ = new AST_binary_expr(getbinop(string($2)), $1, $3);}
		| '(' expr ')' {$$ = $2;}
		| location {$$ = new AST_location_expr($1);}
		| NUM {$$ = new AST_num_expr($1);}
		;

/*
expr	: 	expr '+' expr 
	|	expr '*' expr 
	| 	NUMBER
	|	IDENTIFIER
	;
*/

%%

enum BinaryOP getbinop(string s){
    switch(s[0]){
    case '+':
	return plus_op;
	break;
    case '-':
	return minus_op;
	break;
    case '*':
	return mul_op;
	break;
    case '/':
	return div_op;
	break;
    default:
	dprintf(2, "Unknown binary operator %s\tDefaulting to +\n", s.c_str());
	return plus_op;
    }
}

enum CmpOp getcmpop(string s){
    if(s.length() == 1){
	if(s[0] == '<')
	    return lt_op;
	if(s[0] == '>')
	    return gt_op;
    }
    else if(s.length() == 2){
	if(s[0] == '<' && s[1] == '=')
	    return leq_op;
	if(s[0] == '>' && s[1] == '=')
	    return geq_op;
	if(s[0] == '=' && s[1] == '=')
	    return eq_op;
	if(s[0] == '!' && s[1] == '=')
	    return neq_op;
    }
    dprintf(2, "Unknown comparision operator %s\tDefaulting to ==\n", s.c_str());
    return eq_op;
}

enum LogicOP getlogicop(string s){
    if(s[0] == '&' && s[1] == '&')
	return and_op;
    if(s[0] == '|' && s[1] == '|')
	return or_op;
    dprintf(2, "Unknown logical operator %s\tDefaulting to &&\n", s.c_str());
    return and_op;
}

void yyerror(char const *s){
    dprintf(2, "EEK, parse error! %s\n", s);
}
