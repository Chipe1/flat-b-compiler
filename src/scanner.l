%{
#include<stdlib.h>
#include<string.h>

#include "ast.h"
#include "parser.tab.h"

#define YY_DECL extern "C" int yylex()
%}

%option nounput

%%
"declblock"		return declaration_starter;
"codeblock"		return code_starter;
[0-9][0-9]*		{yylval.ival = atoi(yytext);return NUM; }

\"[ !#-~]*\"	{yylval.sval = strdup(yytext);return STR_LIT;}

"int"		return INT;
"while"		return WHILE;
"for"		return FOR;
"if"		return IF;
"else"		return ELSE;
"goto"		return GOTO;
"got"		return GOT;
"print"		return PRINT;
"println"	return PRINTLN;
"read"		return READ;

[a-zA-Z][a-zA-Z0-9]*   {yylval.sval = strdup(yytext);return ID;}

"+"|"-"|"*"		{yylval.sval = strdup(yytext);return A_OP;}
"=="|"!="|"<"|">"|"<="|">="    {yylval.sval = strdup(yytext);return C_OP;}
"&&"|"||"    {yylval.sval = strdup(yytext);return L_OP;}
"("		return '(';
")"		return ')';
"{"		return '{';
"}"		return '}';
"["		return '[';
"]"		return ']';
"="		return '=';
","		return ',';
":"		return ':';
";"             return ';';

[ \t\n]		{ /* Do nothing */ }
.		{ 
		  printf("Unexpected token encountered: %s\n", yytext); 
		  return ETOK;
		}
