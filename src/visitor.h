#ifndef __VISITOR_DFN_
#define __VISITOR_DFN_

class Visitor;

#include "ast.h"

class Visitor{
 public:
    Visitor(){}
    ~Visitor(){}
    virtual void* visit(AST_node *) = 0;
    virtual void* visit(AST_program *) = 0;
    virtual void* visit(AST_decl *) = 0;
    virtual void* visit(AST_id *) = 0;
    virtual void* visit(AST_expr *) = 0;
    virtual void* visit(AST_unary_expr *) = 0;
    virtual void* visit(AST_binary_expr *) = 0;
    virtual void* visit(AST_num_expr *) = 0;
    virtual void* visit(AST_location_expr *) = 0;
    virtual void* visit(AST_location *) = 0;
    virtual void* visit(AST_id_location *) = 0;
    virtual void* visit(AST_idx_location *) = 0;
    virtual void* visit(AST_bool_expr *) = 0;
    virtual void* visit(AST_bool_cmp_expr *) = 0;
    virtual void* visit(AST_bool_logic_expr *) = 0;
    virtual void* visit(AST_label *) = 0;
    virtual void* visit(AST_IO *) = 0;
    virtual void* visit(AST_print_statement *) = 0;
    virtual void* visit(AST_println_statement *) = 0;
    virtual void* visit(AST_read_statement *) = 0;
    virtual void* visit(AST_print *) = 0;
    virtual void* visit(AST_print_str *) = 0;
    virtual void* visit(AST_print_expr *) = 0;
    virtual void* visit(AST_statement *) = 0;
    virtual void* visit(AST_assign *) = 0;
    virtual void* visit(AST_expr_statement *) = 0;
    virtual void* visit(AST_io_statement *) = 0;
    virtual void* visit(AST_codeblock *) = 0;
    virtual void* visit(AST_for *) = 0;
    virtual void* visit(AST_if *) = 0;
    virtual void* visit(AST_while *) = 0;
    virtual void* visit(AST_label_statement *) = 0;
    virtual void* visit(AST_goto *) = 0;
};

#endif
