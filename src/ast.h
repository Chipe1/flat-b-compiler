#ifndef __AST_DFN_
#define __AST_DFN_

#include<vector>
#include<string>

using namespace std;

#define DBG_AST 0

enum Datatype{
    int_type,
    bool_type,
};

enum UnaryOP{
    neg_op,
};

enum BinaryOP{
    plus_op,
    minus_op,
    mul_op,
    div_op,
};

enum CmpOp{
    eq_op,
    neq_op,
    lt_op,
    gt_op,
    leq_op,
    geq_op,
};

enum LogicOP{
    and_op,
    or_op,
};

class AST_node;
class AST_program;
class AST_decl;
class AST_id;
class AST_expr;
class AST_unary_expr;
class AST_binary_expr;
class AST_num_expr;
class AST_location_expr;
class AST_location;
class AST_id_location;
class AST_idx_location;
class AST_bool_expr;
class AST_bool_cmp_expr;
class AST_bool_logic_expr;
class AST_label;
class AST_IO;
class AST_print_statement;
class AST_println_statement;
class AST_read_statement;
class AST_print;
class AST_print_str;
class AST_print_expr;
class AST_statement;
class AST_assign;
class AST_expr_statement;
class AST_io_statement;
class AST_codeblock;
class AST_for;
class AST_if;
class AST_while;
class AST_label_statement;
class AST_goto;

#include "visitor.h"

class AST_node{
 public:
    AST_node();
    ~AST_node();
    virtual void* accept(Visitor *);
};

class AST_program: public AST_node{
 public:
    vector< AST_decl* >* dcl;
    vector< AST_statement* >* cdb;
    AST_program(vector< AST_decl* >*, vector< AST_statement* >*);
    void* accept(Visitor *);
};

class AST_decl: public AST_node{
 public:
    enum Datatype dtype;
    vector< AST_id* >* idl;
    AST_decl(enum Datatype, vector< AST_id* >*);
    void* accept(Visitor *);
};

class AST_id: AST_node{
 public:
    string id;
    int ar_size;
    AST_id(string, int);
    AST_id(string);
    void* accept(Visitor *);
};

class AST_expr: public AST_node{};

class AST_unary_expr: public AST_expr{
 public:
    enum UnaryOP op;
    AST_expr *expr;
    AST_unary_expr(enum UnaryOP, AST_expr *);
    void* accept(Visitor *);
};

class AST_binary_expr: public AST_expr{
 public:
    enum BinaryOP op;
    AST_expr *expr_l;
    AST_expr *expr_r;
    AST_binary_expr(enum BinaryOP, AST_expr *, AST_expr *);
    void* accept(Visitor *);
};

class AST_num_expr: public AST_expr{
 public:
    int val;
    AST_num_expr(int);
    void* accept(Visitor *);
};

class AST_location_expr: public AST_expr{
 public:
    AST_location *location;
    AST_location_expr(AST_location *);
    void* accept(Visitor *);
};


class AST_location: public AST_node{};

class AST_id_location: public AST_location{
 public:
    string id;
    AST_id_location(string);
    void* accept(Visitor *);
};

class AST_idx_location: public AST_location{
 public:
    string id;
    AST_expr *expr;
    AST_idx_location(string, AST_expr *);
    void* accept(Visitor *);
};

class AST_bool_expr: public AST_node{};

class AST_bool_cmp_expr: public AST_bool_expr{
 public:
    enum CmpOp op;
    AST_expr *expr_l;
    AST_expr *expr_r;
    AST_bool_cmp_expr(enum CmpOp, AST_expr *, AST_expr *);
    void* accept(Visitor *);
};

class AST_bool_logic_expr: public AST_bool_expr{
 public:
    enum LogicOP op;
    AST_bool_expr *bexpr_l;
    AST_bool_expr *bexpr_r;
    AST_bool_logic_expr(enum LogicOP, AST_bool_expr *, AST_bool_expr *);
    void* accept(Visitor *);
};

class AST_label: public AST_node{
 public:
    string id;
    AST_label(string);
    void* accept(Visitor *);
};

class AST_IO: public AST_node{};

class AST_print_statement: public AST_IO{
 public:
    vector< AST_print* > *plist;
    AST_print_statement(vector< AST_print* >*);
    void* accept(Visitor *);
};

class AST_println_statement: public AST_IO{
 public:
    vector< AST_print* > *plist;
    AST_println_statement(vector< AST_print* >*);
    void* accept(Visitor *);
};

class AST_read_statement: public AST_IO{
 public:
    AST_location *loc;
    AST_read_statement(AST_location *);
    void* accept(Visitor *);
};

class AST_print: public AST_node{};

class AST_print_str: public AST_print{
 public:
    string s;
    AST_print_str(string);
    void* accept(Visitor *);
};

class AST_print_expr: public AST_print{
 public:
    AST_expr* expr;
    AST_print_expr(AST_expr *);
    void* accept(Visitor *);
};

class AST_statement: public AST_node{};

class AST_assign: public AST_statement{
 public:
    AST_location *location;
    AST_expr *expr;
    AST_assign(AST_location *, AST_expr *);
    void* accept(Visitor *);
};

class AST_expr_statement: public AST_statement{
 public:
    AST_expr *expr;
    AST_expr_statement(AST_expr *);
    void* accept(Visitor *);
};

class AST_io_statement: public AST_statement{
 public:
    AST_IO *io;
    AST_io_statement(AST_IO *);
    void* accept(Visitor *);
};

class AST_codeblock: public AST_statement{
 public:
    vector< AST_statement* > *stlist;
    AST_codeblock(vector< AST_statement* >*);
    void* accept(Visitor *);
};

class AST_for: public AST_statement{
 public:
    string id;
    int start, end, incr;
    vector< AST_statement* > *cdb;
    AST_for(string, int, int, int, vector< AST_statement* >*);
    AST_for(string, int, int, vector< AST_statement* >*);
    void* accept(Visitor *);
};

class AST_if: public AST_statement{
 public:
    AST_bool_expr *cond;
    vector< AST_statement* > *if_cdb;
    vector< AST_statement* > *else_cdb;
    AST_if(AST_bool_expr *, vector< AST_statement* >*, vector< AST_statement* >*);
    AST_if(AST_bool_expr *, vector< AST_statement* >*);
    void* accept(Visitor *);
};

class AST_while: public AST_statement{
 public:
    AST_bool_expr *cond;
    vector< AST_statement* > *cdb;
    AST_while(AST_bool_expr *, vector< AST_statement* >*);
    void* accept(Visitor *);
};

class AST_label_statement: public AST_statement{
 public:
    AST_label *label;
    AST_statement *stmt;
    AST_label_statement(AST_label *, AST_statement *);
    void* accept(Visitor *);
};

class AST_goto: public AST_statement{
 public:
    AST_label *label;
    AST_bool_expr *cond;
    AST_goto(AST_label *, AST_bool_expr *);
    AST_goto(AST_label *);
    void* accept(Visitor *);
};
#endif
