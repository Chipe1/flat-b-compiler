#ifndef __C_CODE_GEN_DFN_
#define __C_CODE_GEN_DFN_

#include<string>
#include<vector>
#include<map>
#include<set>

using namespace std;

#define DBG_CC 0

class C_codegen;

#include "visitor.h"
#include "ast.h"

class C_codegen: public Visitor{
    FILE* fd;
    map< string, pair< enum Datatype, int > > vartable;
    multiset< string > stackvars;
    string body;
 public:
    C_codegen(string);
    ~C_codegen();
    void* visit(AST_node *);
    void* visit(AST_program *);
    void* visit(AST_decl *);
    void* visit(AST_id *);
    void* visit(AST_expr *);
    void* visit(AST_unary_expr *);
    void* visit(AST_binary_expr *);
    void* visit(AST_num_expr *);
    void* visit(AST_location_expr *);
    void* visit(AST_location *);
    void* visit(AST_id_location *);
    void* visit(AST_idx_location *);
    void* visit(AST_bool_expr *);
    void* visit(AST_bool_cmp_expr *);
    void* visit(AST_bool_logic_expr *);
    void* visit(AST_label *);
    void* visit(AST_IO *);
    void* visit(AST_print_statement *);
    void* visit(AST_println_statement *);
    void* visit(AST_read_statement *);
    void* visit(AST_print *);
    void* visit(AST_print_str *);
    void* visit(AST_print_expr *);
    void* visit(AST_statement *);
    void* visit(AST_assign *);
    void* visit(AST_expr_statement *);
    void* visit(AST_io_statement *);
    void* visit(AST_codeblock *);
    void* visit(AST_for *);
    void* visit(AST_if *);
    void* visit(AST_while *);
    void* visit(AST_label_statement *);
    void* visit(AST_goto *);
    string* expand_cdb(vector< AST_statement* > *);
};
#endif
