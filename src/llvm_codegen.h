#ifndef __LLVM_CODE_GEN_DFN_
#define __LLVM_CODE_GEN_DFN_

#include<string>
#include<vector>
#include<map>
#include<set>
#include<llvm/IR/LLVMContext.h>
#include<llvm/IR/BasicBlock.h>

using namespace std;

#define DBG_LC 0

class LLVM_codegen;

#include "visitor.h"
#include "ast.h"

class LLVM_codegen: public Visitor{
    llvm::LLVMContext context;
    llvm::Module *module;
    llvm::Function *mainfunc;
    llvm::BasicBlock *cur_block;
    llvm::Type *i32_t;
    
    map< string, pair< enum Datatype, int > > vartable;
    map< string, llvm::Value* > valtable;
 public:
    LLVM_codegen(string);
    ~LLVM_codegen();
    void* visit(AST_node *);
    void* visit(AST_program *);
    void* visit(AST_decl *);
    void* visit(AST_id *);
    void* visit(AST_expr *);
    void* visit(AST_unary_expr *);
    void* visit(AST_binary_expr *);
    void* visit(AST_num_expr *);
    void* visit(AST_location_expr *);
    void* visit(AST_location *);
    void* visit(AST_id_location *);
    void* visit(AST_idx_location *);
    void* visit(AST_bool_expr *);
    void* visit(AST_bool_cmp_expr *);
    void* visit(AST_bool_logic_expr *);
    void* visit(AST_label *);
    void* visit(AST_IO *);
    void* visit(AST_print_statement *);
    void* visit(AST_println_statement *);
    void* visit(AST_read_statement *);
    void* visit(AST_print *);
    void* visit(AST_print_str *);
    void* visit(AST_print_expr *);
    void* visit(AST_statement *);
    void* visit(AST_assign *);
    void* visit(AST_expr_statement *);
    void* visit(AST_io_statement *);
    void* visit(AST_codeblock *);
    void* visit(AST_for *);
    void* visit(AST_if *);
    void* visit(AST_while *);
    void* visit(AST_label_statement *);
    void* visit(AST_goto *);
    void run_cdb(vector< AST_statement* > *);
};
#endif
