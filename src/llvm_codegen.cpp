#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<vector>
#include<map>
#include<set>
#include<llvm/IR/BasicBlock.h>
#include<llvm/IR/LLVMContext.h>
#include<llvm/IR/Module.h>
#include<llvm/IR/Type.h>
#include<llvm/IR/Function.h>
#include<llvm/IR/Constants.h>
#include<llvm/IR/Instructions.h>
#include<llvm/IR/Attributes.h>
#include<llvm/IR/IRBuilder.h>
#include<llvm/Support/raw_ostream.h>
#include "llvm_codegen.h"

LLVM_codegen::LLVM_codegen(string fname){
    if(DBG_LC)printf("\n\ncodegen started\n");
    module = new llvm::Module(fname, context);
    mainfunc = llvm::Function::Create(llvm::FunctionType::get(llvm::Type::getVoidTy(context), false), llvm::GlobalValue::ExternalLinkage, "main", module);
    cur_block = NULL;
    // Commonly used type(s)
    i32_t = llvm::Type::getInt32Ty(context);
}
LLVM_codegen::~LLVM_codegen(){}
void* LLVM_codegen::visit(AST_node *){dprintf(2, "[Unexpected]Accepted by AST_node\n");return NULL;}
void* LLVM_codegen::visit(AST_program *vtb){
    for(auto x: *(vtb->dcl)){
	x->accept(this);
    }
    cur_block = llvm::BasicBlock::Create(context, "main_block", mainfunc, 0);
    run_cdb(vtb->cdb);
    llvm::ReturnInst::Create(context, cur_block);
    cur_block = NULL;
    module->print(llvm::outs(), NULL);
    return NULL; 
}
void* LLVM_codegen::visit(AST_decl *vtb){
    enum Datatype currtype = vtb->dtype;
    vector< pair< string, int > > vars;
    for(auto x: *(vtb->idl)){
	pair< string, int > *childret;
	childret = ((pair< string, int > *)(x->accept(this)));
	vars.push_back(*childret);
	free(childret);
    }
	
    for(auto x: vars){
	if(DBG_LC)printf("Added var %d %s[%d]\n", currtype, x.first.c_str(), x.second);
	if(vartable.find(x.first) != vartable.end()){
	    dprintf(2, ";Overwriting previous definition of %s\n", x.first.c_str());
	}
	vartable[x.first] = make_pair(currtype, x.second);
	if(currtype == int_type){
	    if(x.second == 0){
		llvm::GlobalVariable *newvar = new llvm::GlobalVariable(*module, i32_t, false, llvm::GlobalValue::CommonLinkage, NULL, "new variable");
		newvar->setInitializer(llvm::ConstantInt::get(i32_t, 0, true));
		valtable[x.first] = newvar;
	    }
	    else{
		llvm::GlobalVariable *newvar = new llvm::GlobalVariable(*module, llvm::ArrayType::get(i32_t, x.second), false, llvm::GlobalValue::CommonLinkage, NULL, "new array");
		newvar->setInitializer(llvm::ConstantAggregateZero::get(llvm::ArrayType::get(i32_t, x.second)));
		valtable[x.first] = newvar;
	    }
	}
	else{
	    dprintf(2, "Unsupported type %d for variable %s[%d]\n", currtype, x.first.c_str(), x.second);
	}
    }
    return NULL;
}
void* LLVM_codegen::visit(AST_id *vtb){
    pair< string, int > *toret = new pair< string, int >();
    toret->first = vtb->id;
    toret->second = vtb->ar_size;
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_expr *vtb){dprintf(2, "[Unexpected]Accepted by AST_expr\n");return NULL;}
void* LLVM_codegen::visit(AST_unary_expr *vtb){
    llvm::Value *toret;
    llvm::Value *childret;
    childret = (llvm::Value *)(vtb->expr->accept(this));
    if(vtb->op == neg_op){
	toret = llvm::BinaryOperator::Create(llvm::Instruction::Sub, llvm::ConstantInt::get(i32_t, 0, true), childret, "unary_neg", cur_block);
    }
    else{
	dprintf(2, "Unsupported unary operand %d\n", vtb->op);
	exit(1);
    }
    if(DBG_LC)printf("unary_expr\n");
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_binary_expr *vtb){
    llvm::Value *toret;
    llvm::Value *l_childret, *r_childret;
    l_childret = (llvm::Value *)(vtb->expr_l->accept(this));
    r_childret = (llvm::Value *)(vtb->expr_r->accept(this));
    if(vtb->op == plus_op){
	toret = llvm::BinaryOperator::Create(llvm::Instruction::Add, l_childret, r_childret, "bin_add", cur_block);
    }
    else if(vtb->op == minus_op){
	toret = llvm::BinaryOperator::Create(llvm::Instruction::Sub, l_childret, r_childret, "bin_sub", cur_block);
    }
    else if(vtb->op == mul_op){
	toret = llvm::BinaryOperator::Create(llvm::Instruction::Mul, l_childret, r_childret, "bin_mul", cur_block);
    }
    else if(vtb->op == div_op){
	toret = llvm::BinaryOperator::Create(llvm::Instruction::SDiv, l_childret, r_childret, "bin_div", cur_block);
    }
    else{
	dprintf(2, "Unsupported binary operand %d\n", vtb->op);
	exit(1);
    }
    if(DBG_LC)printf("binary_expr\n");
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_num_expr *vtb){
    llvm::Value* toret;
    toret = llvm::ConstantInt::get(i32_t, vtb->val, true);
    if(DBG_LC)printf("num_expr: %d\n", vtb->val);
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_location_expr *vtb){
    llvm::Value *toret;
    llvm::Value *childret;
    childret = (llvm::Value *)(vtb->location->accept(this));
    toret = new llvm::LoadInst(childret, "loc_expr", cur_block);
    if(DBG_LC)printf("loc_expr\n");
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_location *vtb){dprintf(2, "[Unexpected]Accepted by AST_location\n");return NULL;}
void* LLVM_codegen::visit(AST_id_location *vtb){
    llvm::Value* toret;
    if(vartable.find(vtb->id) == vartable.end()){
	dprintf(2, "Refernce to unknown variable %s\n", vtb->id.c_str());
	exit(1);
    }
    if(vartable[vtb->id].second != 0){
	dprintf(2, "Need to index array variable %s\n", vtb->id.c_str());
	exit(1);
    }
    toret = valtable[vtb->id];
    if(DBG_LC)printf("id_location of %s\n", vtb->id.c_str());
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_idx_location *vtb){
    llvm::Value* toret;
    llvm::Value* childret;
    vector< llvm::Value* > index;
    if(vartable.find(vtb->id) == vartable.end()){
	dprintf(2, "Refernce to unknown variable %s\n", vtb->id.c_str());
	exit(1);
    }
    if(vartable[vtb->id].second == 0){
	dprintf(2, "Cannot index non-array variable %s\n", vtb->id.c_str());
	exit(1);
    }
    childret = (llvm::Value *)(vtb->expr->accept(this));
    index.push_back(llvm::ConstantInt::get(i32_t, 0, true));
    index.push_back(childret);
    toret = valtable[vtb->id];
    toret = llvm::GetElementPtrInst::CreateInBounds(toret, index, "ast_idx", cur_block);
    if(DBG_LC)printf("idx_location of %s[]\n", vtb->id.c_str());
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_bool_expr *vtb){dprintf(2, "[Unexpected]Accepted by AST_bool_expr\n");return NULL;}
void* LLVM_codegen::visit(AST_bool_cmp_expr *vtb){
    llvm::Value *toret;
    llvm::Value *l_childret, *r_childret;
    l_childret = (llvm::Value *)(vtb->expr_l->accept(this));
    r_childret = (llvm::Value *)(vtb->expr_r->accept(this));
    if(vtb->op == eq_op){
	toret = llvm::CmpInst::Create(llvm::Instruction::ICmp, llvm::ICmpInst::ICMP_EQ, l_childret, r_childret,"cmp_eq", cur_block);
    }
    else if(vtb->op == neq_op){
	toret = llvm::CmpInst::Create(llvm::Instruction::ICmp, llvm::ICmpInst::ICMP_NE, l_childret, r_childret,"cmp_neq", cur_block);
    }
    else if(vtb->op == lt_op){
	toret = llvm::CmpInst::Create(llvm::Instruction::ICmp, llvm::ICmpInst::ICMP_SLT, l_childret, r_childret,"cmp_lt", cur_block);
    }
    else if(vtb->op == gt_op){
	toret = llvm::CmpInst::Create(llvm::Instruction::ICmp, llvm::ICmpInst::ICMP_SGT, l_childret, r_childret,"cmp_gt", cur_block);
    }
    else if(vtb->op == leq_op){
	toret = llvm::CmpInst::Create(llvm::Instruction::ICmp, llvm::ICmpInst::ICMP_SLE, l_childret, r_childret,"cmp_leq", cur_block);
    }
    else if(vtb->op == geq_op){
	toret = llvm::CmpInst::Create(llvm::Instruction::ICmp, llvm::ICmpInst::ICMP_SGE, l_childret, r_childret,"cmp_geq", cur_block);
    }
    else{
	dprintf(2, "Unsupported comparision operator %d\n", vtb->op);
	exit(1);
    }
    if(DBG_LC)printf("bool_cmp_expr\n");
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_bool_logic_expr *vtb){
    llvm::Value *toret;
    llvm::Value *l_childret, *r_childret;
    l_childret = (llvm::Value *)(vtb->bexpr_l->accept(this));
    r_childret = (llvm::Value *)(vtb->bexpr_r->accept(this));
    if(vtb->op == and_op){
	toret = llvm::BinaryOperator::Create(llvm::Instruction::And, l_childret, r_childret, "bool_and", cur_block);
    }
    else if(vtb->op == or_op){
	toret = llvm::BinaryOperator::Create(llvm::Instruction::Or, l_childret, r_childret, "bool_or", cur_block);
    }
    else{
	dprintf(2, "Unsupported logical operator %d\n", vtb->op);
	exit(1);
    }
    if(DBG_LC)printf("bool_logic_expr\n");
    return (void *)toret;	
}
void* LLVM_codegen::visit(AST_label *vtb){dprintf(2, "Label and GOTO not supported in script\tIgnoring\n");return NULL;}
void* LLVM_codegen::visit(AST_IO *vtb){dprintf(2, "[Unexpected]Accepted by AST_IO\n");return NULL;}
void* LLVM_codegen::visit(AST_print_statement *vtb){
    if(DBG_LC)printf("print_stmt;\n");
    vector< llvm::Value* > args;
    string to_print = "";
    args.push_back((llvm::Value *)NULL);
    for(auto x: *(vtb->plist)){
	args.push_back((llvm::Value *)(x->accept(this)));
	if(args.back()->getType() == i32_t){
	    to_print += "%d ";
	}
	else{
	    to_print += "%s ";
	}
    }
    to_print += "\b";
    llvm::Function *printf_func = module->getFunction("printf");
    if(!printf_func){
	if(DBG_LC)printf("Getting printf\n");
        printf_func = llvm::Function::Create(llvm::FunctionType::get(i32_t, true), llvm::GlobalValue::ExternalLinkage, "printf", module);
        printf_func->setCallingConv(llvm::CallingConv::C);
	llvm::AttributeList printf_PAL;
	printf_func->setAttributes(printf_PAL);
    }
    llvm::IRBuilder<> p_builder(context);
    p_builder.SetInsertPoint(cur_block);
    args[0] = p_builder.CreateGlobalStringPtr(to_print.c_str());
    llvm::CallInst::Create(printf_func, args, "printf_call", cur_block);
    return NULL;
}
void* LLVM_codegen::visit(AST_println_statement *vtb){
    if(DBG_LC)printf("println_stmt;\n");
    vector< llvm::Value* > args;
    string to_print = "";
    args.push_back((llvm::Value *)NULL);
    for(auto x: *(vtb->plist)){
	args.push_back((llvm::Value *)(x->accept(this)));
	if(args.back()->getType() == i32_t){
	    to_print += "%d ";
	}
	else{
	    to_print += "%s ";
	}
    }
    to_print += "\b\n";
    llvm::Function *printf_func = module->getFunction("printf");
    if(!printf_func){
	if(DBG_LC)printf("Getting printf\n");
        printf_func = llvm::Function::Create(llvm::FunctionType::get(i32_t, true), llvm::GlobalValue::ExternalLinkage, "printf", module);
        printf_func->setCallingConv(llvm::CallingConv::C);
	llvm::AttributeList printf_PAL;
	printf_func->setAttributes(printf_PAL);
    }
    llvm::IRBuilder<> p_builder(context);
    p_builder.SetInsertPoint(cur_block);
    args[0] = p_builder.CreateGlobalStringPtr(to_print.c_str());
    llvm::CallInst::Create(printf_func, args, "printf_call", cur_block);
    return NULL;
}
void* LLVM_codegen::visit(AST_read_statement *vtb){
    if(DBG_LC)printf("read_stmt;\n");
    vector< llvm::Value* > args;
    llvm::Value *childret;
    string to_read = "%d";
    childret = (llvm::Value *)(vtb->loc->accept(this));
    llvm::Function *scanf_func = module->getFunction("scanf");
    if(!scanf_func){
	if(DBG_LC)printf("Getting scanf\n");
        scanf_func = llvm::Function::Create(llvm::FunctionType::get(i32_t, true), llvm::GlobalValue::ExternalLinkage, "scanf", module);
        scanf_func->setCallingConv(llvm::CallingConv::C);
	llvm::AttributeList printf_PAL;
	scanf_func->setAttributes(printf_PAL);
    }
    llvm::IRBuilder<> p_builder(context);
    p_builder.SetInsertPoint(cur_block);
    args.push_back(p_builder.CreateGlobalStringPtr(to_read.c_str()));
    args.push_back(childret);
    llvm::CallInst::Create(scanf_func, args, "scanf_call", cur_block);
    return NULL;    
}
void* LLVM_codegen::visit(AST_print *vtb){dprintf(2, "[Unexpected]Accepted by AST_print\n");return NULL;}
void* LLVM_codegen::visit(AST_print_str *vtb){
    llvm::Value *toret;
    llvm::IRBuilder<> p_builder(context);
    p_builder.SetInsertPoint(cur_block);
    toret = p_builder.CreateGlobalStringPtr(vtb->s.c_str());
    if(DBG_LC)printf("print_str\n");
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_print_expr *vtb){
    llvm::Value *toret;
    llvm::Value *childret;
    childret = (llvm::Value *)(vtb->expr->accept(this));
    toret = childret;
    if(DBG_LC)printf("print_expr\n");
    return (void *)toret;
}
void* LLVM_codegen::visit(AST_statement *vtb){dprintf(2, "[Unexpected]Accepted by AST_statement\n");return NULL;}
void* LLVM_codegen::visit(AST_assign *vtb){
    llvm::Value *toret;
    llvm::Value *exprret;
    toret = (llvm::Value *)(vtb->location->accept(this));
    exprret = (llvm::Value *)(vtb->expr->accept(this));
    new llvm::StoreInst(exprret, toret, false, cur_block);
    if(DBG_LC)printf("assign\n");
    return NULL;
}
void* LLVM_codegen::visit(AST_expr_statement *vtb){
    if(DBG_LC)printf("expr_stmt;\n");
    vtb->expr->accept(this);
    return NULL;
}
void* LLVM_codegen::visit(AST_io_statement *vtb){
    if(DBG_LC)printf("io_stmt;\n");
    vtb->io->accept(this);
    return NULL;
}
void* LLVM_codegen::visit(AST_codeblock *vtb){
    if(DBG_LC)printf("codeblock{}\n");
    this->run_cdb(vtb->stlist);
    return NULL;
}
void* LLVM_codegen::visit(AST_for *vtb){
    if(vartable.find(vtb->id) == vartable.end()){
	dprintf(2, "Loop variable %s Undefined\n", vtb->id.c_str());
	exit(1);
    }
    else if(vartable[vtb->id].second != 0){
	dprintf(2, "Loop variable %s must be not be an array\n", vtb->id.c_str());
	exit(1);
    }
    llvm::Value *condret, *tmp;
    llvm::BasicBlock *check_block, *loop_block, *break_block;
    // do initialization in the old block itself
    new llvm::StoreInst(llvm::ConstantInt::get(i32_t, vtb->start, true), valtable[vtb->id], false, cur_block);
    check_block = llvm::BasicBlock::Create(context, "check", cur_block->getParent());
    llvm::BranchInst::Create(check_block, cur_block);

    // checking block
    cur_block = check_block;
    loop_block = llvm::BasicBlock::Create(context, "loop", cur_block->getParent());
    break_block = llvm::BasicBlock::Create(context, "return", cur_block->getParent());
    tmp = new llvm::LoadInst(valtable[vtb->id], "chk_load", cur_block);
    if(vtb->incr > 0){
	// if incr > 0, then chech if loop_var < end
	condret = new llvm::ICmpInst(*check_block, llvm::ICmpInst::ICMP_SLT, tmp, llvm::ConstantInt::get(i32_t, vtb->end, true), "pos_incr_cmp");
    }
    else{
	// else incr < 0, so chech if loop_var > end
	condret = new llvm::ICmpInst(*check_block, llvm::ICmpInst::ICMP_SGT, tmp, llvm::ConstantInt::get(i32_t, vtb->end, true), "neg_incr_cmp");
    }
    llvm::BranchInst::Create(loop_block, break_block, condret, cur_block);

    // loop block
    cur_block = loop_block;
    this->run_cdb(vtb->cdb);

    // increment
    // tmp = new llvm::LoadInst(valtable[vtb->id], "chk_load", cur_block);  // Comment this to ignore changes in loop variable
    tmp = llvm::BinaryOperator::Create(llvm::Instruction::Add, tmp, llvm::ConstantInt::get(i32_t, vtb->incr, true), "incr", cur_block);
    new llvm::StoreInst(tmp, valtable[vtb->id], false, cur_block);
    llvm::BranchInst::Create(check_block, cur_block);

    // loop break
    cur_block = break_block;
    
    return NULL;
}
void* LLVM_codegen::visit(AST_if *vtb){
    llvm::Value *condret;
    llvm::BasicBlock *if_block, *else_block, *merge_block;
    if_block = llvm::BasicBlock::Create(context, "if", cur_block->getParent());
    merge_block = llvm::BasicBlock::Create(context, "merge", cur_block->getParent());
    else_block = llvm::BasicBlock::Create(context, "else", cur_block->getParent());

    // checking and branch
    condret = (llvm::Value *)(vtb->cond->accept(this));
    llvm::BranchInst::Create(if_block, else_block, condret, cur_block);

    // if block
    cur_block = if_block;
    this->run_cdb(vtb->if_cdb);
    llvm::BranchInst::Create(merge_block, cur_block);

    // else block
    cur_block = else_block;
    this->run_cdb(vtb->else_cdb);
    llvm::BranchInst::Create(merge_block, cur_block);

    // merge here
    cur_block = merge_block;
    return NULL;
}
void* LLVM_codegen::visit(AST_while *vtb){
    llvm::Value *condret;
    llvm::BasicBlock *loop_block, *check_block, *break_block;
    check_block = llvm::BasicBlock::Create(context, "check", cur_block->getParent());
    loop_block = llvm::BasicBlock::Create(context, "loop", cur_block->getParent());
    break_block = llvm::BasicBlock::Create(context, "outofloop", cur_block->getParent());
    llvm::BranchInst::Create(check_block, cur_block);

    // check block
    cur_block = check_block;
    condret = (llvm::Value *)(vtb->cond->accept(this));
    llvm::BranchInst::Create(loop_block, break_block, condret, cur_block);

    // loop block
    cur_block = loop_block;
    this->run_cdb(vtb->cdb);
    llvm::BranchInst::Create(check_block, cur_block);

    // break block(no break statment invloved)
    cur_block = break_block;
    return NULL; 
}
void* LLVM_codegen::visit(AST_label_statement *vtb){
    dprintf(2, "Ignoring label in script\n");
    if(DBG_LC)printf("label_stmt: \n");
    vtb->stmt->accept(this);
    return NULL;
}
void* LLVM_codegen::visit(AST_goto *vtb){
    dprintf(2, "GOTO not supported\tIgnoring\n");
    return NULL;
}

void LLVM_codegen::run_cdb(vector< AST_statement* > *cdb){
    for(auto x: *cdb){
	x->accept(this);
    }
}
