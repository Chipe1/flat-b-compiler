#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<vector>
#include "ast.h"

using namespace std;

AST_node::AST_node(){}
AST_node::~AST_node(){}
void* AST_node::accept(Visitor *visitor){
    printf("Visited AST_node\nUnimplemented\n");
    return visitor->visit(this);
}

AST_program::AST_program(vector< AST_decl* >* dcl, vector< AST_statement* >* cdb){
    if(DBG_AST)printf("prog\n");
    this->dcl = dcl;
    this->cdb = cdb;
}
void* AST_program::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_program\n");
    return visitor->visit(this);
}

AST_decl::AST_decl(enum Datatype dtype, vector< AST_id* >* idl){
    if(DBG_AST)printf("decl\n");
    this->dtype = dtype;
    this->idl = idl;
}
void* AST_decl::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_decl\n");
    return visitor->visit(this);
}

AST_id::AST_id(string id, int ar_size){
    if(DBG_AST)printf("id: %s[%d]\n", id.c_str(), ar_size);
    this->id = id;
    if(ar_size < 1){
	dprintf(2, "Invalid size %d for %s\tdefaulting to size(1)\n", ar_size, id.c_str());
	ar_size = 1;
    }
    this->ar_size = ar_size;
}
AST_id::AST_id(string id){
    if(DBG_AST)printf("id: %s\n", id.c_str());
    this->id = id;
    this->ar_size = 0;
}
void* AST_id::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_id\n");
    return visitor->visit(this);
}

AST_unary_expr::AST_unary_expr(enum UnaryOP op, AST_expr *expr){
    if(DBG_AST)printf("unary: %d\n", op);
    this->op = op;
    this->expr = expr;
}
void* AST_unary_expr::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_unary\n");
    return visitor->visit(this);
}

AST_binary_expr::AST_binary_expr(enum BinaryOP op, AST_expr *expr_l, AST_expr *expr_r){
    if(DBG_AST)printf("binary: %d\n", op);
    this->op = op;
    this->expr_l = expr_l;
    this->expr_r = expr_r;
}
void* AST_binary_expr::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_binary\n");
    return visitor->visit(this);
}

AST_num_expr::AST_num_expr(int val){
    if(DBG_AST)printf("numexpr: %d\n", val);
    this->val = val;
}
void* AST_num_expr::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_num_expr\n");
    return visitor->visit(this);
}

AST_location_expr::AST_location_expr(AST_location *location){
    if(DBG_AST)printf("loc_expr\n");
    this->location = location;
}
void* AST_location_expr::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_loc_expr\n");
    return visitor->visit(this);
}

AST_id_location::AST_id_location(string id){
    if(DBG_AST)printf("id_loc: %s\n", id.c_str());
    this->id = id;
}
void* AST_id_location::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_id_loc\n");
    return visitor->visit(this);
}

AST_idx_location::AST_idx_location(string id, AST_expr *expr){
    if(DBG_AST)printf("idx_loc: %s\n", id.c_str());
    this->id = id;
    this->expr = expr;
}
void* AST_idx_location::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_idx_loc\n");
    return visitor->visit(this);
}

AST_bool_cmp_expr::AST_bool_cmp_expr(enum CmpOp op, AST_expr *expr_l, AST_expr *expr_r){
    if(DBG_AST)printf("bool_cmp: %d\n", op);
    this->op = op;
    this->expr_l = expr_l;
    this->expr_r = expr_r;
}
void* AST_bool_cmp_expr::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_bool_cmp\n");
    return visitor->visit(this);
}

AST_bool_logic_expr::AST_bool_logic_expr(enum LogicOP op, AST_bool_expr *bexpr_l, AST_bool_expr *bexpr_r){
    if(DBG_AST)printf("bool_logic: %d\n", op);
    this->op = op;
    this->bexpr_l = bexpr_l;
    this->bexpr_r = bexpr_r;
}
void* AST_bool_logic_expr::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_bool_logic\n");
    return visitor->visit(this);
}

AST_label::AST_label(string id){
    if(DBG_AST)printf("label: %s\n", id.c_str());
    this->id = id;
}
void* AST_label::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_label\n");
    return visitor->visit(this);
}

AST_print_statement::AST_print_statement(vector< AST_print* > *plist){
    if(DBG_AST)printf("print_statement\n");
    this->plist = plist;
}
void* AST_print_statement::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_print_stmt\n");
    return visitor->visit(this);
}

AST_println_statement::AST_println_statement(vector< AST_print* > *plist){
    if(DBG_AST)printf("println_statement\n");
    this->plist = plist;
}
void* AST_println_statement::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_println_stmt\n");
    return visitor->visit(this);
}

AST_read_statement::AST_read_statement(AST_location *loc){
    if(DBG_AST)printf("read_statement\n");
    this->loc = loc;
}
void* AST_read_statement::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_read\n");
    return visitor->visit(this);
}

AST_print_str::AST_print_str(string s){
    if(DBG_AST)printf("print_str: %s\n", s.c_str());
    this->s = s.substr(1, s.length() - 2);
}
void* AST_print_str::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_print_str\n");
    return visitor->visit(this);
}

AST_print_expr::AST_print_expr(AST_expr *expr){
    if(DBG_AST)printf("printexpr\n");
    this->expr = expr;
}
void* AST_print_expr::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_print_expr\n");
    return visitor->visit(this);
}

AST_assign::AST_assign(AST_location *location, AST_expr *expr){
    if(DBG_AST)printf("assign\n");
    this->location = location;
    this->expr = expr;
}
void* AST_assign::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_assign\n");
    return visitor->visit(this);
}

AST_expr_statement::AST_expr_statement(AST_expr *expr){
    if(DBG_AST)printf("expr stmt\n");
    this->expr = expr;
}
void* AST_expr_statement::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_expr_stmt\n");
    return visitor->visit(this);
}

AST_io_statement::AST_io_statement(AST_IO *io){
    if(DBG_AST)printf("IOstmt\n");
    this->io = io;
}
void* AST_io_statement::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_io_stmt\n");
    return visitor->visit(this);
}

AST_codeblock::AST_codeblock(vector< AST_statement* > *stlist){
    if(DBG_AST)printf("cdb\n");
    this->stlist = stlist;
}
void* AST_codeblock::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_cdb\n");
    return visitor->visit(this);
}

AST_for::AST_for(string id, int start, int end, int incr, vector< AST_statement* > *cdb){
    if(DBG_AST)printf("For %s: [%d, %d) delta=%d\n", id.c_str(), start, end, incr);
    this->id = id;
    this->start = start;
    this->end = end;
    if((start > end && incr >= 0) || (start < end && incr <= 0)){
	dprintf(2, "Infinite loop? %s: [%d, %d) delta=%d\n", id.c_str(), start, end, incr);
    }
    this->incr = incr;
    this->cdb = cdb;
}
AST_for::AST_for(string id, int start, int end, vector< AST_statement* > *cdb){
    if(DBG_AST)printf("For %s: [%d, %d)\n", id.c_str(), start, end);
    this->id = id;
    this->start = start;
    this->end = end;
    this->incr = 1;
    this->cdb = cdb;
}
void* AST_for::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_for\n");
    return visitor->visit(this);
}

AST_if::AST_if(AST_bool_expr *cond, vector< AST_statement* > *if_cdb, vector< AST_statement* > *else_cdb){
    if(DBG_AST)printf("ifnoelse\n");
    this->cond = cond;
    this->if_cdb = if_cdb;
    this->else_cdb = else_cdb;
}
AST_if::AST_if(AST_bool_expr *cond, vector< AST_statement* > *if_cdb){
    if(DBG_AST)printf("ifwithelse\n");
    this->cond = cond;
    this->if_cdb = if_cdb;
    this->else_cdb = new vector< AST_statement* >();
}
void* AST_if::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_if\n");
    return visitor->visit(this);
}

AST_while::AST_while(AST_bool_expr *cond, vector< AST_statement* > *cdb){
    if(DBG_AST)printf("while\n");
    this->cond = cond;
    this->cdb = cdb;
}
void* AST_while::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_while\n");
    return visitor->visit(this);
}

AST_label_statement::AST_label_statement(AST_label *label, AST_statement *stmt){
    if(DBG_AST)printf("label stmt\n");
    this->label = label;
    this->stmt = stmt;
}
void* AST_label_statement::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_label_stmt\n");
    return visitor->visit(this);
}

AST_goto::AST_goto(AST_label *label, AST_bool_expr *cond){
    if(DBG_AST)printf("goto cond\n");
    this->label = label;
    this-> cond = cond;
}
AST_goto::AST_goto(AST_label *label){
    if(DBG_AST)printf("goto nocond\n");
    this->label = label;
    this->cond = NULL;
}
void* AST_goto::accept(Visitor *visitor){
    if(DBG_AST)printf("accept AST_goto\n");
    return visitor->visit(this);
}
