#ifndef __C_CODE_GEN_DFN_
#define __C_CODE_GEN_DFN_

#include<string>
#include<vector>
#include<map>

using namespace std;

#define DBG_BS 0

class Bscript;

#include "visitor.h"
#include "ast.h"

class Bscript: public Visitor{
    map< string, pair< enum Datatype, int > > vartable;
    map< string, void* > valtable;
 public:
    Bscript();
    ~Bscript();
    void* visit(AST_node *);
    void* visit(AST_program *);
    void* visit(AST_decl *);
    void* visit(AST_id *);
    void* visit(AST_expr *);
    void* visit(AST_unary_expr *);
    void* visit(AST_binary_expr *);
    void* visit(AST_num_expr *);
    void* visit(AST_location_expr *);
    void* visit(AST_location *);
    void* visit(AST_id_location *);
    void* visit(AST_idx_location *);
    void* visit(AST_bool_expr *);
    void* visit(AST_bool_cmp_expr *);
    void* visit(AST_bool_logic_expr *);
    void* visit(AST_label *);
    void* visit(AST_IO *);
    void* visit(AST_print_statement *);
    void* visit(AST_println_statement *);
    void* visit(AST_read_statement *);
    void* visit(AST_print *);
    void* visit(AST_print_str *);
    void* visit(AST_print_expr *);
    void* visit(AST_statement *);
    void* visit(AST_assign *);
    void* visit(AST_expr_statement *);
    void* visit(AST_io_statement *);
    void* visit(AST_codeblock *);
    void* visit(AST_for *);
    void* visit(AST_if *);
    void* visit(AST_while *);
    void* visit(AST_label_statement *);
    void* visit(AST_goto *);
    void run_cdb(vector< AST_statement* >*);
};
#endif
