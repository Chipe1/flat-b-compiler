#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<vector>
#include<map>
#include "bscript.h"

Bscript::Bscript(){
    if(DBG_BS)printf("\n\nbscript started\n");
}
Bscript::~Bscript(){}
void* Bscript::visit(AST_node *){dprintf(2, "[Unexpected]Accepted by AST_node\n");return NULL;}
void* Bscript::visit(AST_program *vtb){
    for(auto x: *(vtb->dcl)){
	x->accept(this);
    }
    run_cdb(vtb->cdb);
    return NULL; 
}
void* Bscript::visit(AST_decl *vtb){
    enum Datatype currtype = vtb->dtype;
    vector< pair< string, int > > vars;
    for(auto x: *(vtb->idl)){
	pair< string, int > *childret;
	childret = ((pair< string, int > *)(x->accept(this)));
	vars.push_back(*childret);
	free(childret);
    }
	
    for(auto x: vars){
	if(DBG_BS)printf("Added var %d %s[%d]\n", currtype, x.first.c_str(), x.second);
	if(vartable.find(x.first) != vartable.end()){
	    dprintf(2, "Overwriting previous definition of %s\n", x.first.c_str());
	}
	vartable[x.first] = make_pair(currtype, x.second);
	if(currtype == int_type){
	    valtable[x.first] = malloc(sizeof(int)*max(x.second, 1));
	}
	else{
	    dprintf(2, "Unsupported type %d for variable %s[%d]\n", currtype, x.first.c_str(), x.second);
	}
    }
    return NULL;
}
void* Bscript::visit(AST_id *vtb){
    pair< string, int > *toret = new pair< string, int >();
    toret->first = vtb->id;
    toret->second = vtb->ar_size;
    return (void *)toret;
}
void* Bscript::visit(AST_expr *vtb){dprintf(2, "[Unexpected]Accepted by AST_expr\n");return NULL;}
void* Bscript::visit(AST_unary_expr *vtb){
    long long toret;
    long long childret;
    childret = (long long)(vtb->expr->accept(this));
    if(vtb->op == neg_op){
	toret = -(int)childret;
    }
    else{
	dprintf(2, "Unsupported unary operand %d\n", vtb->op);
	exit(1);
    }
    if(DBG_BS)printf("unary_expr: %d\n", (int)toret);
    return (void *)toret;
}
void* Bscript::visit(AST_binary_expr *vtb){
    long long toret;
    long long l_childret, r_childret;
    l_childret = (long long)(vtb->expr_l->accept(this));
    r_childret = (long long)(vtb->expr_r->accept(this));
    if(vtb->op == plus_op){
	toret = (int)l_childret + (int)r_childret;
    }
    else if(vtb->op == minus_op){
	toret = (int)l_childret - (int)r_childret;
    }
    else if(vtb->op == mul_op){
	toret = (int)l_childret * (int)r_childret;
    }
    else if(vtb->op == div_op){
	toret = (int)l_childret / (int)r_childret;
    }
    else{
	dprintf(2, "Unsupported binary operand %d\n", vtb->op);
	exit(1);
    }
    if(DBG_BS)printf("binary_expr: %d\n", (int)toret);
    return (void *)toret;
}
void* Bscript::visit(AST_num_expr *vtb){
    long long toret;
    toret = vtb->val;
    if(DBG_BS)printf("num_expr: %d\n", (int)toret);
    return (void *)toret;
}
void* Bscript::visit(AST_location_expr *vtb){
    long long toret;
    int* childret;
    childret = (int *)(vtb->location->accept(this));
    toret = (long long)(*childret);
    if(DBG_BS)printf("loc_expr: %d\n", (int)toret);
    return (void *)toret;
}
void* Bscript::visit(AST_location *vtb){dprintf(2, "[Unexpected]Accepted by AST_location\n");return NULL;}
void* Bscript::visit(AST_id_location *vtb){
    int* toret;
    if(vartable.find(vtb->id) == vartable.end()){
	dprintf(2, "Refernce to unknown variable %s\n", vtb->id.c_str());
	exit(1);
    }
    if(vartable[vtb->id].second != 0){
	dprintf(2, "Need to index array variable %s\n", vtb->id.c_str());
	exit(1);
    }
    toret = (int *)valtable[vtb->id];
    if(DBG_BS)printf("id_location of %s\n", vtb->id.c_str());
    return (void *)toret;
}
void* Bscript::visit(AST_idx_location *vtb){
    int* toret;
    long long childret;
    childret = (long long)(vtb->expr->accept(this));
    if(vartable.find(vtb->id) == vartable.end()){
	dprintf(2, "Refernce to unknown variable %s\n", vtb->id.c_str());
	exit(1);
    }
    if(vartable[vtb->id].second == 0){
	dprintf(2, "Cannot to index non-array variable %s\n", vtb->id.c_str());
	exit(1);
    }
    toret = (int *)valtable[vtb->id] + childret;
    if(DBG_BS)printf("idx_location of %s[%d]\n", vtb->id.c_str(), (int)childret);
    return (void *)toret;
}
void* Bscript::visit(AST_bool_expr *vtb){dprintf(2, "[Unexpected]Accepted by AST_bool_expr\n");return NULL;}
void* Bscript::visit(AST_bool_cmp_expr *vtb){
    long long toret;
    long long l_childret, r_childret;
    l_childret = (long long)(vtb->expr_l->accept(this));
    r_childret = (long long)(vtb->expr_r->accept(this));
    if(vtb->op == eq_op){
	toret = (l_childret == r_childret);
    }
    else if(vtb->op == neq_op){
	toret = (l_childret != r_childret);
    }
    else if(vtb->op == lt_op){
	toret = (l_childret < r_childret);
    }
    else if(vtb->op == gt_op){
	toret = (l_childret > r_childret);
    }
    else if(vtb->op == leq_op){
	toret = (l_childret <= r_childret);
    }
    else if(vtb->op == geq_op){
	toret = (l_childret >= r_childret);
    }
    else{
	dprintf(2, "Unsupported comparision operator %d\n", vtb->op);
	exit(1);
    }
    if(DBG_BS)printf("bool_cmp_expr: %lld\n", toret);
    return (void *)toret;
}
void* Bscript::visit(AST_bool_logic_expr *vtb){
    long long toret;
    long long l_childret, r_childret;
    l_childret = (long long)(vtb->bexpr_l->accept(this));
    if(vtb->op == and_op){
	if(l_childret){
	    r_childret = (long long)(vtb->bexpr_r->accept(this));
	    toret  = r_childret;
	}
	else{
	    toret = 0;
	}
    }
    else if(vtb->op == or_op){
	if(l_childret){
	    toret = 1;
	}
	else{
	    r_childret = (long long)(vtb->bexpr_r->accept(this));
	    toret  = r_childret;
	}
    }
    else{
	dprintf(2, "Unsupported logical operator %d\n", vtb->op);
	exit(1);
    }
    if(DBG_BS)printf("bool_logic_expr: %lld\n", toret);
    return (void *)toret;	
}
void* Bscript::visit(AST_label *vtb){dprintf(2, "Label and GOTO not supported in script\tIgnoring\n");return NULL;}
void* Bscript::visit(AST_IO *vtb){dprintf(2, "[Unexpected]Accepted by AST_IO\n");return NULL;}
void* Bscript::visit(AST_print_statement *vtb){
    if(DBG_BS)printf("print_stmt;\n");
    for(auto x: *(vtb->plist)){
	x->accept(this);
	printf(" ");
    }
    printf("\b");
    return NULL;
}
void* Bscript::visit(AST_println_statement *vtb){
    if(DBG_BS)printf("println_stmt;\n");
    for(auto x: *(vtb->plist)){
	x->accept(this);
	printf(" ");
    }
    printf("\b\n");
    return NULL;
}
void* Bscript::visit(AST_read_statement *vtb){
    int* childret;
    childret = (int *)(vtb->loc->accept(this));
    scanf("%d", childret);
    if(DBG_BS)printf("read_stmt read %d\n", *childret);
    return NULL;
}
void* Bscript::visit(AST_print *vtb){dprintf(2, "[Unexpected]Accepted by AST_print\n");return NULL;}
void* Bscript::visit(AST_print_str *vtb){
    printf("%s", vtb->s.c_str());
    if(DBG_BS)printf("print_str: %s\n", vtb->s.c_str());
    return NULL;
}
void* Bscript::visit(AST_print_expr *vtb){
    long long childret;
    childret = (long long)(vtb->expr->accept(this));
    printf("%d", (int)childret);
    if(DBG_BS)printf("print_expr: %lld\n", childret);
    return NULL;
}
void* Bscript::visit(AST_statement *vtb){dprintf(2, "[Unexpected]Accepted by AST_statement\n");return NULL;}
void* Bscript::visit(AST_assign *vtb){
    int *locret;
    long long exprret;
    locret = (int *)(vtb->location->accept(this));
    exprret = (long long)(vtb->expr->accept(this));
    *locret = (int)exprret;
    if(DBG_BS)printf("assign: %d\n", (int)exprret);
    return NULL;
}
void* Bscript::visit(AST_expr_statement *vtb){
    if(DBG_BS)printf("expr_stmt;\n");
    vtb->expr->accept(this);
    return NULL;
}
void* Bscript::visit(AST_io_statement *vtb){
    if(DBG_BS)printf("io_stmt;\n");
    vtb->io->accept(this);
    return NULL;
}
void* Bscript::visit(AST_codeblock *vtb){
    if(DBG_BS)printf("codeblock{}\n");
    this->run_cdb(vtb->stlist);
    return NULL;
}
void* Bscript::visit(AST_for *vtb){
    int stack_preced, old_val;
    int i;

    if(DBG_BS)printf("for()\n");
    // manage variable declaration
    if(vartable.find(vtb->id) != vartable.end()){
	if(vartable[vtb->id].first != int_type || vartable[vtb->id].second != 0){
	    dprintf(2, "Variable used in for loop must be a new variable or of INT type\n");
	    exit(1);
	}
	stack_preced = 1;
	old_val = *((int *)(valtable[vtb->id]));
    }
    else{
	stack_preced = 0;
	vartable[vtb->id] = make_pair(int_type, 0);
	valtable[vtb->id] = malloc(sizeof(int));
    }

    // main loop
    for(i = vtb->start; i < vtb->end; i += vtb->incr){
	*((int *)(valtable[vtb->id])) = i;
	this->run_cdb(vtb->cdb);
    }

    // clean up variable
    if(stack_preced){
	*((int *)(valtable[vtb->id])) = old_val;
    }
    else{
	free(valtable[vtb->id]);
	vartable.erase(vtb->id);
	valtable.erase(vtb->id);
    }
    
    return NULL;
}
void* Bscript::visit(AST_if *vtb){
    long long condret;
    condret = (long long)(vtb->cond->accept(this));
    if(DBG_BS)printf("if: %lld\n", condret);
    if(condret){
	this->run_cdb(vtb->if_cdb);
    }
    else{
	this->run_cdb(vtb->else_cdb);
    }
    return NULL;
}
void* Bscript::visit(AST_while *vtb){
    long long condret;
    if(DBG_BS)printf("while:\n");
    while(142857){
	condret = (long long)(vtb->cond->accept(this));
	if(DBG_BS)printf("while->condret: %lld\n", condret);
	if(!condret)break;
	this->run_cdb(vtb->cdb);
    }
    return NULL;
}
void* Bscript::visit(AST_label_statement *vtb){
    dprintf(2, "Ignoring label in script\n");
    if(DBG_BS)printf("label_stmt: \n");
    vtb->stmt->accept(this);
    return NULL;
}
void* Bscript::visit(AST_goto *vtb){
    dprintf(2, "GOTO not supported in script\tIgnoring\n");
    return NULL;
}

void Bscript::run_cdb(vector< AST_statement* > *cdb){
    for(auto x: *cdb){
	x->accept(this);
    }
}
