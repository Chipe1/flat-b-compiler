#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<vector>
#include<map>
#include<set>
#include "c_codegen.h"

C_codegen::C_codegen(string fname){
    if(DBG_CC)printf("\n\ncodegen started\n");
    fd = fopen(fname.c_str(), "w");
    body = "";
}
C_codegen::~C_codegen(){}
void* C_codegen::visit(AST_node *){dprintf(2, "[Unexpected]Accepted by AST_node\n");return NULL;}
void* C_codegen::visit(AST_program *vtb){
    string *childret;
    for(auto x: *(vtb->dcl)){
	x->accept(this);
    }
    body = "#include<stdio.h>\n";
    for(auto x: vartable){
	if(x.second.first == int_type){
	    body += "int ";
	}
	else{
	    dprintf(2, "Unknown type %d for variable %s\tIgnoring\n", x.second.first, x.first.c_str());
	}

	body += x.first;
	if(x.second.second > 0){
	    body += "[" + to_string(x.second.second) + "]";
	}
	body += ";\n";
    }

    body += "int main(void)";
    childret = expand_cdb(vtb->cdb);
    body += *childret;
    body += "\0";
    free(childret);
    fwrite((const void *)body.c_str(), sizeof(char), body.length(), fd);
    return NULL; 
}
void* C_codegen::visit(AST_decl *vtb){
    enum Datatype currtype = vtb->dtype;
    vector< pair< string, int > > vars;
    for(auto x: *(vtb->idl)){
	pair< string, int > *childret;
	childret = ((pair< string, int > *)(x->accept(this)));
	vars.push_back(*childret);
	free(childret);
    }
	
    for(auto x: vars){
	if(DBG_CC)printf("Added var %d %s[%d]\n", currtype, x.first.c_str(), x.second);
	if(vartable.find(x.first) != vartable.end()){
	    dprintf(2, "Overwriting previous definition of %s\n", x.first.c_str());
	}
	vartable[x.first] = make_pair(currtype, x.second);
    }
    return NULL;
}
void* C_codegen::visit(AST_id *vtb){
    pair< string, int > *toret = new pair< string, int >();
    toret->first = vtb->id;
    toret->second = vtb->ar_size;
    return (void *)toret;
}
void* C_codegen::visit(AST_expr *vtb){dprintf(2, "[Unexpected]Accepted by AST_expr\n");return NULL;}
void* C_codegen::visit(AST_unary_expr *vtb){
    string *toret = new string();
    string *childret;
    childret = (string *)(vtb->expr->accept(this));
    if(vtb->op == neg_op){
	*toret = "-" + *childret;
    }
    else{
	dprintf(2, "Unsupported unary operand %d\n", vtb->op);
	exit(1);
    }
    free(childret);
    *toret = "(" + (*toret) + ")";
    if(DBG_CC)printf("unary_expr: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_binary_expr *vtb){
    string *toret = new string();
    string *l_childret, *r_childret;
    l_childret = (string *)(vtb->expr_l->accept(this));
    r_childret = (string *)(vtb->expr_r->accept(this));
    if(vtb->op == plus_op){
	*toret = (*l_childret) + "+" + (*r_childret);
    }
    else if(vtb->op == minus_op){
	*toret = (*l_childret) + "-" + (*r_childret);
    }
    else if(vtb->op == mul_op){
	*toret = (*l_childret) + "*" + (*r_childret);
    }
    else if(vtb->op == div_op){
	*toret = (*l_childret) + "/" + (*r_childret);
    }
    else{
	dprintf(2, "Unsupported binary operand %d\n", vtb->op);
	exit(1);
    }
    free(l_childret);
    free(r_childret);
    *toret = "(" + (*toret) + ")";
    if(DBG_CC)printf("binary_expr: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_num_expr *vtb){
    string *toret = new string();
    *toret = to_string(vtb->val);
    *toret = "(" + (*toret) + ")";
    if(DBG_CC)printf("num_expr: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_location_expr *vtb){
    string *toret = new string();
    string *childret;
    childret = (string *)(vtb->location->accept(this));
    toret = childret;
    *toret = "(" + (*toret) + ")";
    if(DBG_CC)printf("loc_expr: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_location *vtb){dprintf(2, "[Unexpected]Accepted by AST_location\n");return NULL;}
void* C_codegen::visit(AST_id_location *vtb){
    string *toret = new string();
    *toret = vtb->id;
    if(stackvars.find(*toret) == stackvars.end() && vartable.find(*toret) == vartable.end()){
	dprintf(2, "Refernce to unknown variable %s\n", toret->c_str());
	exit(1);
    }
    if(vartable[*toret].second != 0){
	dprintf(2, "Need to index array variable %s\n", toret->c_str());
	exit(1);
    }
    *toret = "(" + (*toret) + ")";
    if(DBG_CC)printf("id_location: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_idx_location *vtb){
    string *toret = new string();
    string *childret;
    childret = (string *)(vtb->expr->accept(this));
    *toret = vtb->id;
    if(vartable.find(*toret) == vartable.end()){
	dprintf(2, "Refernce to unknown variable %s\n", toret->c_str());
	exit(1);
    }
    if(vartable[*toret].second == 0){
	dprintf(2, "Cannot to index non-array variable %s\n", toret->c_str());
	exit(1);
    }
    *toret = *toret + "[" + *childret + "]";
    free(childret);
    *toret = "(" + (*toret) + ")";
    if(DBG_CC)printf("idx_location: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_bool_expr *vtb){dprintf(2, "[Unexpected]Accepted by AST_bool_expr\n");return NULL;}
void* C_codegen::visit(AST_bool_cmp_expr *vtb){
    string *toret = new string();
    string *l_childret, *r_childret;
    l_childret = (string *)(vtb->expr_l->accept(this));
    r_childret = (string *)(vtb->expr_r->accept(this));
    if(vtb->op == eq_op){
	*toret = (*l_childret) + "==" + (*r_childret);
    }
    else if(vtb->op == neq_op){
	*toret = (*l_childret) + "!=" + (*r_childret);
    }
    else if(vtb->op == lt_op){
	*toret = (*l_childret) + "<" + (*r_childret);
    }
    else if(vtb->op == gt_op){
	*toret = (*l_childret) + ">" + (*r_childret);
    }
    else if(vtb->op == leq_op){
	*toret = (*l_childret) + "<=" + (*r_childret);
    }
    else if(vtb->op == geq_op){
	*toret = (*l_childret) + ">=" + (*r_childret);
    }
    else{
	dprintf(2, "Unsupported comparision operator %d\n", vtb->op);
	exit(1);
    }
    free(l_childret);
    free(r_childret);
    *toret = "(" + (*toret) + ")";
    if(DBG_CC)printf("bool_cmp_expr: %s\n", toret->c_str());
    return (void *)toret;	
}
void* C_codegen::visit(AST_bool_logic_expr *vtb){
    string *toret = new string();
    string *l_childret, *r_childret;
    l_childret = (string *)(vtb->bexpr_l->accept(this));
    r_childret = (string *)(vtb->bexpr_r->accept(this));
    if(vtb->op == and_op){
	*toret = (*l_childret) + "&&" + (*r_childret);
    }
    else if(vtb->op == or_op){
	*toret = (*l_childret) + "||" + (*r_childret);
    }
    else{
	dprintf(2, "Unsupported logical operator %d\n", vtb->op);
	exit(1);
    }
    free(l_childret);
    free(r_childret);
    *toret = "(" + (*toret) + ")";
    if(DBG_CC)printf("bool_logic_expr: %s\n", toret->c_str());
    return (void *)toret;	
}
void* C_codegen::visit(AST_label *vtb){
    string *toret = new string();
    *toret = vtb->id;
    if(DBG_CC)printf("ast_label: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_IO *vtb){dprintf(2, "[Unexpected]Accepted by AST_IO\n");return NULL;}
void* C_codegen::visit(AST_print_statement *vtb){
    string *toret = new string();
    vector< pair< int, string > > to_print;
    for(auto x: *(vtb->plist)){
	pair< int, string > *childret;
	childret = (pair< int, string > *)(x->accept(this));
	to_print.push_back(*childret);
	free(childret);
    }
    string args = "";
    *toret = "printf(\"";
    for(auto x: to_print){
	if(x.first == 0){
	    // expr
	    (*toret) += "%d, ";
	    args += ", "+x.second;
	}
	else if(x.first == 1){
	    // string literal
	    (*toret) += "%s, ";
	    args += ", "+x.second;
	}
	else{
	    dprintf(2, "Bad return pair from AST_print: %d\n", x.first);
	    exit(1);
	}
    }
    *toret += "\"" + args + ")";
    if(DBG_CC)printf("print_stmt: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_println_statement *vtb){
    string *toret = new string();
    vector< pair< int, string > > to_print;
    for(auto x: *(vtb->plist)){
	pair< int, string > *childret;
	childret = (pair< int, string > *)(x->accept(this));
	to_print.push_back(*childret);
	free(childret);
    }
    string args = "";
    *toret = "printf(\"";
    for(auto x: to_print){
	if(x.first == 0){
	    // expr
	    (*toret) += "%d, ";
	    args += ", "+x.second;
	}
	else{
	    // string literal
	    (*toret) += "%s, ";
	    args += ", "+x.second;
	}
    }
    *toret += "\\n\"" + args + ")";
    if(DBG_CC)printf("print_stmt: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_read_statement *vtb){
    string *toret = new string();
    string *childret;
    childret = (string *)(vtb->loc->accept(this));
    *toret = "scanf(\"%d\", &" + (*childret) + ")";
    free(childret);
    if(DBG_CC)printf("read_stmt: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_print *vtb){dprintf(2, "[Unexpected]Accepted by AST_print\n");return NULL;}
void* C_codegen::visit(AST_print_str *vtb){
    pair< int, string > *toret = new pair< int, string >();
    toret->first = 1;
    toret->second = vtb->s;
    if(DBG_CC)printf("print_str: %s\n", (toret->second).c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_print_expr *vtb){
    pair< int, string > *toret = new pair< int, string >();
    string *childret;
    toret->first = 0;
    childret = (string *)(vtb->expr->accept(this));
    toret->second = *childret;
    free(childret);
    if(DBG_CC)printf("print_expr: %s\n", (toret->second).c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_statement *vtb){dprintf(2, "[Unexpected]Accepted by AST_statement\n");return NULL;}
void* C_codegen::visit(AST_assign *vtb){
    string *toret = new string();
    string *locret, *exprret;
    locret = (string *)(vtb->location->accept(this));
    exprret = (string *)(vtb->expr->accept(this));
    *toret = (*locret) + " = " + (*exprret) + ";\n";
    free(locret);
    free(exprret);
    if(DBG_CC)printf("assign: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_expr_statement *vtb){
    string *toret = new string();
    string *childret;
    childret = (string *)(vtb->expr->accept(this));
    *toret = *childret + ";\n";
    free(childret);
    if(DBG_CC)printf("expr_stmt: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_io_statement *vtb){
    string *toret = new string();
    string *childret;
    childret = (string *)(vtb->io->accept(this));
    *toret = *childret + ";\n";
    free(childret);
    if(DBG_CC)printf("expr_stmt: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_codeblock *vtb){
    string *toret = this->expand_cdb(vtb->stlist);
    return (void *)toret;
}
void* C_codegen::visit(AST_for *vtb){
    string *toret = new string();
    string *childret;
    (this->stackvars).insert(vtb->id);
    (*toret) = "{\nint " + vtb->id + ";\n";
    (*toret) += "for(" + vtb->id  + " = " + to_string(vtb->start) + ";" + vtb->id + " < " + to_string(vtb->end) + "; " + vtb->id + " += " + to_string(vtb->incr) + ")\n";
    childret = this->expand_cdb(vtb->cdb);
    (*toret) += *childret;
    (*toret) += "\n}";
    free(childret);
    (this->stackvars).erase(vtb->id);
    if(DBG_CC)printf("for: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_if *vtb){
    string *toret = new string();
    string *condret, *ifret, *elseret;
    condret = (string *)(vtb->cond->accept(this));
    ifret = this->expand_cdb(vtb->if_cdb);
    elseret = this->expand_cdb(vtb->else_cdb);
    *toret = "if(" + (*condret) + ")" + (*ifret) + "else" + (*elseret);
    free(condret);
    free(ifret);
    free(elseret);
    if(DBG_CC)printf("if: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_while *vtb){
    string *toret = new string();
    string *condret, *cdbret;
    condret = (string *)(vtb->cond->accept(this));
    cdbret = this->expand_cdb(vtb->cdb);
    *toret = "while(" + (*condret) + ")" + (*cdbret);
    free(condret);
    free(cdbret);
    if(DBG_CC)printf("if: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_label_statement *vtb){
    string *toret = new string();
    string *labelret, *stmtret;
    labelret = (string *)(vtb->label->accept(this));
    stmtret = (string *)(vtb->stmt->accept(this));
    *toret = (*labelret) + ":\n" + (*stmtret);
    free(labelret);
    free(stmtret);	
    if(DBG_CC)printf("label_stmt: %s\n", toret->c_str());
    return (void *)toret;
}
void* C_codegen::visit(AST_goto *vtb){
    string *toret = new string();
    string *condret, gotostmt, *labelret;
    labelret = (string *)(vtb->label->accept(this));
    gotostmt = "{goto " + (*labelret) + ";}\n";
    if(vtb->cond){
	condret = (string *)(vtb->cond->accept(this));
    }
    else{
	condret = new string("1 == 1");
    }
    *toret = "if(" + (*condret) + ")" + gotostmt;
    free(condret);
    if(DBG_CC)printf("goto_stmt: %s\n", toret->c_str());
    return (void *)toret;
}

string* C_codegen::expand_cdb(vector< AST_statement* > *cdb){
    string *toret = new string();
    *toret = "{\n";
    for(auto x: *cdb){
	string *childret;
	childret = (string *)(x->accept(this));
	(*toret) += *childret;
	free(childret);
    }
    (*toret) += "}\n";
    if(DBG_CC)printf("expand_cdb: %s\n", toret->c_str());
    return toret;
}
