declblock{
	int i, j;
}

codeblock{
	i = 1;j = 4;
	if i == 1 && j < 4{
	   println "Must NOT";
	}
	else{
	   println "Must";
	   if 1 == 2{
              println "Must NOT";
	   }
           else{
              println "Must";
           }
	}

	if (i < 3) && (j >= i){
	   println "Must";
	}
	else{
	   println "Must NOT";
	}

	if i != j || i == j{
	   println "Must";
	}
}
