declblock{
	int i, j, k, ar[100], n;
}

codeblock{
	println "Array size?";
	read n;
	i = 0;
	while i < n{
	      read ar[i];
	      i = i + 1;
	}
	i = 0;
	while i < n{
	      j = 0;
	      while j < (n - 1){
	      	    if ar[j] < ar[j+1]{
		       k = ar[j + 1];
		       ar[j + 1] = ar[j];
		       ar[j] = k;
		    }
		    j = j + 1;
	      }
	      i = i + 1;
	}
	println "Sorted array";
	i = 0;
	while i < n{
	      print ar[i], "";
	      i = i + 1;
	}
	println "";
}
