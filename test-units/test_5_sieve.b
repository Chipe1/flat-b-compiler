declblock{
	int i, j, k, s[1000000];
}

codeblock{
	for i = 0, 1000000{
		s[i] = 0;
	}
	for i = 2, 1000000{
		if(s[i] == 0){
			j = i*2;
			while j < 1000000{
				s[j] = 1;
				j = j + i;
			}
		}
	}
}
